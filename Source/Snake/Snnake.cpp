// Fill out your copyright notice in the Description page of Project Settings.


#include "Snnake.h"
#include "SnakeElementBase.h"

// Sets default values
ASnnake::ASnnake()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	ElementSize = 60.f;
	MovementSpeed = 1.f;
	LastMoveDirection = EMovementDirection::DOWN;

}

// Called when the game starts or when spawned
void ASnnake::BeginPlay()
{
	Super::BeginPlay();
	SetActorTickInterval(MovementSpeed);
	AddSnakeElement(5);
	
	
}

// Called every frame
void ASnnake::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	Move();

}

void ASnnake::AddSnakeElement(int ElementsNum)
{
	for (int i = 0;i < ElementsNum; i++)
	{
	FVector NewLocation(SnakeElements.Num() * ElementSize, 0, 0);
	FTransform NewTransform(NewLocation);
	ASnakeElementBase*NewSnakeElem = GetWorld()->SpawnActor<ASnakeElementBase>(SnakeElementClass,NewTransform);	
	
	int32 ElemIndex = SnakeElements.Add(NewSnakeElem);
	if (ElemIndex == 0)
	{
		NewSnakeElem->SetFirstElementType();


	}


	}
	

}

void ASnnake::Move()
{
	FVector MovementVector(ForceInitToZero);
	float MovementSpeedDelta = ElementSize;
	

	switch (LastMoveDirection)
	{
	case EMovementDirection::UP:
		MovementVector.X += MovementSpeedDelta;
		break;
	case EMovementDirection::DOWN:
			MovementVector.X -= MovementSpeedDelta;
			break;
	case EMovementDirection::LEFT:
		MovementVector.Y += MovementSpeedDelta;
		break;
	case EMovementDirection::RIGHT:
		MovementVector.Y -= MovementSpeedDelta;
		break;


	}
	//AddActorWorldOffset(MovementVector);

	for (int i = SnakeElements.Num() - 1; i > 0; i--)
	{
		auto CurrentElement = SnakeElements[i];
		auto PrevElement = SnakeElements[i - 1];
		FVector PrevLocation = PrevElement->GetActorLocation();
		CurrentElement->SetActorLocation(PrevLocation);

	}
	SnakeElements[0]->AddActorWorldOffset(MovementVector);
}

